/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package feathergo

// Get gets the SVG representation of a Feather Icon. An empty string
// will be returned, if no icon exists of the given ID.
func Get(id string) string {
	svg, ok := _icons[id]
	if !ok {
		return ""
	}
	return svg
}
