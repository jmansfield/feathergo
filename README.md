feathergo
===

feathergo is a Go library for accessing [Feather icons] from within Go applications,
specifically intended for inlining icons within Go web applications.

feathergo currently targets Feather Icons **v4.29.0**.

## Licence

feathergo itself is licenced under the BSD 2-Clause License. The SVG icons themselves
are licenced [MIT][feather-licence].

[Feather icons]: https://feathericons.com/
[feather-licence]: https://github.com/feathericons/feather/blob/master/LICENSE
